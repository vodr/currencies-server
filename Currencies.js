'use strict';

class Currencies {

	constructor(io) {
	    this.popularity = {};
	    this.io = io;
  	}

  	//if has stored rates for current date then will return them otherwise will get them from API
	getRates (base){
		var currentDate = this.getCurrentDate();
		if(this.ratesDate === currentDate){ //already loaded some date for current date
			if(!this.rates[base]){ //checks if the base currency is loaded
				this.rates[base] = this.loadRates(base);
			}
		}else{ //new date, cleans all stored data
			this.ratesDate = currentDate;
			this.rates = {};
			this.rates[base] = this.loadRates(base);
		}

		this.updatePopularity(base);
		return this.rates[base]
	}


	getCurrencies (){
		if(!this.currencies){
			if(this.rates){ //Tries to use data from previous usage of getRates()
				var rates = this.rates[Object.keys(this.rates)[0]];
				
			}else{ //No data to use, just loads rates for EUR
				var rates = this.loadRates("EUR");
			}
			this.setupCurrencies(rates);
		}
		return this.currencies;
	}

	//creates currencies list from given rates
	setupCurrencies (rates){
		this.currencies = [rates.base];
		for(var i in rates.rates){
			this.currencies.push(i);
		}
		this.currencies = this.currencies.sort();
	}

	updatePopularity(base){
		if(!this.popularity[base]){
			this.popularity[base] = 1;
		}
		else{
			this.popularity[base]++;
		}

		//if there is a new most popular base currency, then emit it
		var mostPopular = this.getMostPopular();
		if(!this.mostPopular || this.mostPopular !== mostPopular ){
			this.io.emit('newMostPopular', mostPopular);
		}
	}

	//return most popular base currency
	getMostPopular(){
		var maxI = null;
		var max = null;
		for(var i in this.popularity){
			if(max === null || this.popularity[i] > max){
				maxI = i;
				max = this.popularity[i]
			}
		}
		return maxI;
	}

	//loads rates form API
	loadRates (base){
		var request = require('sync-request');
		var url = 'http://api.fixer.io/latest?base=' + base;
		var res = request('GET', url);
		return JSON.parse(res.getBody('utf8')); //could be checked if object has expected structure
	}

	//return current date in format YYYY-MM-DD
	getCurrentDate (){
		var date = new Date();
		var month = date.getMonth() + 1; // month is zero based
		var day = date.getDate();
		return date.getFullYear() + "-" 
			+ (month >= 10 ? month : "0" + month) + "-" 
			+ (day >= 10 ? day : "0" + day);
	}
}

module.exports = Currencies;