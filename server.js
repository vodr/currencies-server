'use strict';

const Hapi = require('hapi');
var Joi = require('joi');


// Create a server with a host and port
const server = new Hapi.Server();  
server.connection({  
    port: 3000
});


//setup socekt ip
var io = require('socket.io')(server.listener);
io.on('connection', function(socket){
  
});


//setup currencies class
var Currencies = require('./Currencies');
var currencies = new Currencies(io);


//#### ROUTES ####

// Add the route
server.route({  
    method: 'GET',
    path:'/rates/{base}',
    handler: function (request, reply) {
            return reply(currencies.getRates(request.params.base));
    },
    config: {    
        validate: {
            params: {
                base: Joi.string().length(3)
            }
        },
        cors: true
    }
});

server.route({  
    method: 'GET',
    path:'/currencies',
    handler: function (request, reply) {
            return reply({
                currencies: currencies.getCurrencies(),
                mostPopular: currencies.getMostPopular()
            });
    },
    config: {    
        cors: true
    }
});

// Start the server
server.start((err) => {

    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});